<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='icon' type='image/jpg' href="anuncio.jpg">
    <title>Anúncios</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="base.css" media="screen">
    <link rel="stylesheet" href="https://code.google.com/p/css3-mediaqueries-js">
    <script src="js/jquery-1.7.2.min.js"></script>

    <link href="css/lightbox.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css"/>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo"></script>

</head>


<body>

<header>Anúncios</header>

<input type="checkbox" id="chk">
<label for="chk" c class="menu-icon">&#9776&nbsp;&nbsp;&nbsp;Menu</label>

<div class="bg"></div>

<nav class="menu" id="principal">

    <ul>
        <li><a href="">Voltar</a></li>
        <li><a href="index.php">Home</a></li>
        <li><a href="cadastrando.php">Cadastre-se</a></li>
        <li><a href="#" data-toggle="modal" data-target="#myModalcad">Fazer Login</a></li>
        <li><a href="produto.php">Anúncios</a></li>
    </ul>

</nav><br><br><br><br><br><br>


<div class="container theme-showcase" role="main">
    <div class="page-header">
        <h1>Anúncios</h1>
    </div>
    <!-- Inicio Modal -->
    <div class="modal fade" id="myModalcad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Login</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="login.php" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nome:</label>
                            <input name="nome" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="text" class="control-label">Senha:</label>
                            <input name="senha" type="password" class="form-control">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Logar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><br><br>



    <div id="meuSlider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#meuSlider" data-slide-to="0" class="active"></li>
            <li data-target="#meuSlider" data-slide-to="1"></li>
            <li data-target="#meuSlider" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active"><h3>Notebook</h3><img src="notebook1.jpg"  alt="Slider 1" style="margin:0 auto;"/></div>
            <div class="item"><h3>Video Game</h3><img src="videogame.jpg" alt="Slide 2" style="margin:0 auto;" /></div>
            <div class="item"><h3>Televisores</h3><img src="tv.jpg" alt="Slide 3" style="margin:0 auto;"/></div>
        </div>
        <a class="left carousel-control" href="#meuSlider" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#meuSlider" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    <br><br>


    <form name="signup" method="post" action="cadastrando.php">
        <input type="submit" class="btn btn-light" value="Cadastre-se"><br>
    </form>

    <h2>Informações ao vivo da NASA</h2>
    <img id="apod_img_id" width="250px"/>

    <iframe id="apod_vid_id" type="text/html" width="640" height="385" frameborder="0"></iframe>
    <p id="copyright"></p>

    <h3 id="apod_title"></h3>
    <p id="apod_explaination"></p>


    <script>
        var url = "https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo";


        $.ajax({
            url: url,
            success: function(result){
                if("copyright" in result) {
                    $("#copyright").text("Image Credits: " + result.copyright);
                }
                else {
                    $("#copyright").text("Image Credits: " + "Public Domain");
                }

                if(result.media_type == "video") {
                    $("#apod_img_id").css("display", "none");
                    $("#apod_vid_id").attr("src", result.url);
                }
                else {
                    $("#apod_vid_id").css("display", "none");
                    $("#apod_img_id").attr("src", result.url);
                }
                $("#reqObject").text(url);
                $("#returnObject").text(JSON.stringify(result, null, 4));
                $("#apod_explaination").text(result.explanation);
                $("#apod_title").text(result.title);
            }
        });
    </script>




    <footer>
        <section class="nb-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-single">
                            <!-- 	<img src="images/logo.png" class="img-responsive" alt="Logo"> -->

                            <!-- This is only for better view of theme if you want your image logo remove div dummy-logo bellow and replace your logo in logo.png and uncomment logo tag above-->
                            <div class="dummy-logo">
                                <div class="icon pull-left brand">
                                    <i class="fa fa-copy"></i>
                                </div>
                                <h2>Anúncios</h2>
                                <p>Cadastre seus anúncios</p>
                            </div>

                            <p></p>
                            <a href="" class="btn btn-footer">Leia mais</a>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="footer-single useful-links">
                            <div class="footer-title"><h2>Navegue</h2></div>
                            <ul class="list-unstyled">
                                <li><a href="index.php">Home <i class="fa fa-angle-right pull-right"></i></a></li>
                                <li><a href="about1.html">Sobre <i class="fa fa-angle-right pull-right"></i></a></li>
                                <li><a href="service1.html">Serviços <i class="fa fa-angle-right pull-right"></i></a></li>
                                <li><a href="contact1.html">Contato <i class="fa fa-angle-right pull-right"></i></a></li>
                        </div>
                    </div>
                    <div class="clearfix visible-sm"></div>

                    <div class="col-md-3 col-sm-6">

                        <div class="col-sm-12 left-clear right-clear footer-single footer-project">
                            <div></div>
                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">

                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">

                            </div>

                        </div>

                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="footer-single">
                            <div class="footer-title"><h2>Endereço de contato</h2></div>
                            <address>
                                323, Bairro, Juiz de Fora<br>
                                MG, Brasil <br>
                                <i class="fa fa-phone"></i>  987 654 3210 <br>
                                <i class="fa fa-fax"></i> 012 123 2345<br>
                                <i class="fa fa-envelope"></i> danielcdequeiroz@gmail.com<br>
                            </address>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="nb-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 copyrt xs-center">
                        2017 © All Rights Reserved. <a href="">Terms & Conditions</a> | <a href="">Privacy Policy</a>
                    </div>
                    <div class="col-sm-6 text-right xs-center">
                        <ul class="list-inline footer-social">
                            <li><a href="https://www.facebook.com/groups/anunciojf/"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.youtube.com/watch?v=VLylO9noUJs&t=17s"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </footer>
</body>
</html>

