<?php

session_start();

include("banco.php");
$query= "Select * from usuario";
$resultado = mysqli_query($conn1,$query);


if(!isset($_SESSION['nome'])) {
    header('location:index.php');
}
?>

<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='icon' type='image/jpg' href="anuncio.jpg">
<title>Painel Administrador</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="base.css" media="screen">
<link rel="stylesheet" href="https://code.google.com/p/css3-mediaqueries-js">
<script src="js/jquery-1.7.2.min.js"></script>

<link href="css/lightbox.css" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo"></script>

<style>

    body{
        font-size: 20px;
        background-image: url(background.jpg);
        background-repeat: no-repeat;
        background-position: absolute;
        background-size: cover;
        height: auto;
    }

    img{
        max-width:1000px;
        max-height:800px;
        margin-left: 0%;
        margin-top: 0%;
        margin-right: 0%;
        margin-bottom: 0%;

    }

    table tr td{
        height: 40px;
        width: 65px;
    }


    .container{
        width:100%;
        top: 0;
    }


    .control-label{
        color:black;
    }
    h1{
        text-align: center;
        color:white;
    }
    h2{
        color:white;
    }
    h3{
        color:white;
        text-align: center;
    }
    p{
        color:white;
    }

    .nb-footer{
        background: #272727;
        margin-top: 60px;
        padding-bottom: 30px;
    }
    .nb-footer .footer-single{
        margin-top: 30px;
    }
    .nb-footer .footer-title{
        display: block;
        margin: 10px 0 25px 0;
        border-bottom: 1px dotted #e4e9f0;
    }
    .nb-footer .footer-single a{
        text-decoration: none;
    }

    .nb-footer .footer-single h2{
        color: #eee;
        font-size: 18px;
        font-weight: 200;
        display: inline-block;
        border-bottom: 2px solid #7BF;
        padding-bottom: 5px;
        margin-bottom: -2px;
    }
    .nb-footer .footer-single li{
        border-top: solid 1px #353535;
    }
    .nb-footer .footer-single li:first-child{
        border-top: none;
    }
    .nb-footer .footer-single li a{
        color: #979797;
        font-size: 12px;
        padding: 6px 0px;
        display: block;
        transition:all 0.4s ease-in-out;
    }
    .nb-footer .footer-single li a:hover{
        color: #7BF;
    }
    .nb-footer .footer-single li a:hover i{
        color: #7BF;
    }
    .nb-footer .dummy-logo {
        margin-top: 11px;
        padding-bottom: 9px;
    }
    .nb-footer .dummy-logo .icon {
        margin-right: 10px;
        border-radius: 20px;
        margin-top: 24px;
    }
    .nb-footer .brand {
        background: #7BF;
    }
    .nb-footer .dummy-logo i {
        font-size: 50px;
        color: #fff;
        padding: 5px;
    }
    .nb-footer .dummy-logo p {
        color: #999;
        font-size: 12px;
    }
    .nb-footer .dummy-logo h2 {
        font-size: 24px !important;
        border-bottom: none;
        color: #696969;
        padding: 5px 0;
    }
    .nb-footer .btn-footer{
        border: 1px solid #7BF;
        margin-top: 10px;
        color: #999;
    }
    .nb-footer .btn-footer:hover{
        background: #7BF;
        color: #fff;
        transition:all 0.4s ease-in-out;

    }
    .nb-footer .useful-links li a{
        text-transform: uppercase;
    }
    .nb-footer .footer-project a{
        font-size: 13px;
    }
    .nb-footer .footer-project img{
        margin-bottom: 20px;
        border: 1px solid #666;
        border-radius: 6px;
        padding: 1px;
        opacity: 0.7;
        transition:all 0.4s ease-in-out;
    }
    .nb-footer .footer-project img:hover{
        opacity: 1.0;
        cursor: pointer;
    }
    .nb-footer .footer-project .footer-title{
        margin-top: 0;
    }
    .nb-footer .footer-single p, .footer-single address{
        color: #979797;
        font-size: 14px;
        margin-top: 5px;
        line-height: 22px;
    }
    .nb-copyright{
        background: #171717;
        padding-bottom: 10px;
    }
    .nb-copyright .copyrt{
        margin-top: 22px;
        font-size: 14px;
    }
    .nb-copyright .copyrt a{
        color: #7BF;
    }
    .nb-copyright .footer-social{
        margin-top: 10px;
    }
    .nb-copyright .footer-social i{
        padding: 5px 10px;
        color: #999;
        border: 1px solid #333;
        margin-top: 10px;
        font-size: 20px;
        border-radius: 5px;
        transition:all 0.4s ease-in-out;
    }
    .nb-copyright .footer-social i:hover{
        background: #7BF;
        color: #fff;
    }
    .nb-copyright .footer-social .fa-facebook{
        padding: 5px 14px;
    }

</style>

<head>
    <meta charset="utf-8"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Tela do Usuário</title>

    <style type="text/css">
        .menu ul li{
            background-color:#6da3f9;
            color:white;
            float:left;
            width: 180px;
            height: 40px;
            display: inline-block;
            text-align: center;
            line-height:40px;
            font-size:25px;
            position: relative;

        }
        .menu ul li:hover{
            background-color: #2c69cd;
        }
        .menu ul{
            padding:0;
        }
        .menu ul ul{
            display: none;
        }
        .menu ul li:hover>ul{
            display:block;
        }
        .menu ul ul ul{
            margin-left: 202px;
            top: 0px;
            position:absolute;
        }
        .menu ul ul ul li:hover{
            background-color: #011638;
        }
    </style>

<body>

    <nav class="menu">
        <ul>
            <li>Exibir usuário
                <ul>
                    <li><?php echo $_SESSION['nome']; ?></li>
                </ul>
            </li>
            <li>Listar usuários
                <?php while($exibe = mysqli_fetch_assoc($resultado)) { ?>
                    <ul>
                        <li><?php echo $exibe['nome']; ?></li></a>
                    </ul>
                <?php } ?>
            </li>
        </ul>
        </ul>
    </nav>

    <br><br><br>

    <div class="botao">
        <form action="cadastrarproduto.php" method="post" name="menuForm">
            <input type="submit" value="Cadastrar produto"><br><br><br>
        </form>

    <form action="deslogar.php">
        <button class="btn btn-secondary">SAIR</button>
    </form>

    <br><br><br>
        <footer>
            <section class="nb-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="footer-single">
                                <!-- 	<img src="images/logo.png" class="img-responsive" alt="Logo"> -->

                                <!-- This is only for better view of theme if you want your image logo remove div dummy-logo bellow and replace your logo in logo.png and uncomment logo tag above-->
                                <div class="dummy-logo">
                                    <div class="icon pull-left brand">
                                        <i class="fa fa-copy"></i>
                                    </div>
                                    <h2>Anúncios</h2>
                                    <p>Cadastre seus anúncios</p>
                                </div>

                                <p></p>
                                <a href="" class="btn btn-footer">Leia mais</a>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="footer-single useful-links">
                                <div class="footer-title"><h2>Navegue</h2></div>
                                <ul class="list-unstyled">
                                    <li><a href="index.php">Home <i class="fa fa-angle-right pull-right"></i></a></li>
                                    <li><a href="about1.html">Sobre <i class="fa fa-angle-right pull-right"></i></a></li>
                                    <li><a href="service1.html">Serviços <i class="fa fa-angle-right pull-right"></i></a></li>
                                    <li><a href="contact1.html">Contato <i class="fa fa-angle-right pull-right"></i></a></li>
                            </div>
                        </div>
                        <div class="clearfix visible-sm"></div>

                        <div class="col-md-3 col-sm-6">

                            <div class="col-sm-12 left-clear right-clear footer-single footer-project">
                                <div></div>
                                <div class="col-xs-4">

                                </div>
                                <div class="col-xs-4">

                                </div>
                                <div class="col-xs-4">

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-4">

                                </div>
                                <div class="col-xs-4">

                                </div>
                                <div class="col-xs-4">

                                </div>

                            </div>

                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="footer-single">
                                <div class="footer-title"><h2>Endereço de contato</h2></div>
                                <address>
                                    323, Bairro, Juiz de Fora<br>
                                    MG, Brasil <br>
                                    <i class="fa fa-phone"></i>  987 654 3210 <br>
                                    <i class="fa fa-fax"></i> 012 123 2345<br>
                                    <i class="fa fa-envelope"></i> danielcdequeiroz@gmail.com<br>
                                </address>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="nb-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 copyrt xs-center">
                            2017 © All Rights Reserved. <a href="">Terms & Conditions</a> | <a href="">Privacy Policy</a>
                        </div>
                        <div class="col-sm-6 text-right xs-center">
                            <ul class="list-inline footer-social">
                                <li><a href="https://www.facebook.com/groups/anunciojf/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.youtube.com/watch?v=VLylO9noUJs&t=17s"><i class="fa fa-youtube-play"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                <li></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </footer>
</body>
</html>