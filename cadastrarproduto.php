<?php
session_start();

$userid = $_SESSION['userid'];
include("banco.php");
mb_internal_encoding("UTF-8");
mb_http_output( "iso-8859-1" );
ob_start("mb_output_handler");
header("Content-Type: text/html; charset=ISO-8859-1",true);
?>

<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width">
<title>Cadastrar Produto</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="base.css" media="screen">
<link rel="stylesheet" href="https://code.google.com/p/css3-mediaqueries-js">
<link rel="stylesheet" href="https://jquerymobile.com/">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
    body{
        font-size: 20px;
        background-image: url(background.jpg);
        background-repeat: no-repeat;
        background-position: absolute;
        background-size: cover;
        height: auto;
    }

    img{
        max-width:1000px;
        max-height:800px;
        margin-left: 0%;
        margin-top: 0%;
        margin-right: 0%;
        margin-bottom: 0%;

    }

    table tr td{
        height: 40px;
        width: 65px;
    }


    .container{
        width:100%;
        top: 0;
    }


    .control-label{
        color:white;
    }
    h1{
        text-align: center;
        color:white;
    }
    h2{
        color:white;
    }
    h3{
        color:white;
        text-align: center;
    }
    p{
        color:white;
    }
    header{
        position: fixed;
        width: 100%;
        height: 50px;
        top:0;
        left:0;
        background-color: #222;
        color:white;
    }
    .bg{
        width: 100%;
        height: 100%;
        left:0;
        top:0;
        position:fixed;
        background-color: rgba(0,0,0,.6);
        display: none;
    }

    .menu-icon{
        position: fixed;
        font-size:25px;
        font-weight: bold;
        padding: 5px;
        width: 40px;
        height: 40px;
        text-align: center;
        background-color: #222;
        color: #fff;
        cursor:pointer;
        transition: all .4s;
        left: 200px;
        top:0;
    }

    .menu-icon:hover{
        background-color: #fff;
        color: #5b859a;
    }

    #chk{
        display:none;

    }
    #chk:checked ~ bg{
        display: block;
    }
    #chk:checked ~ #principal{
        transform: translatex(300px);
    }
    .menu{
        height: 100%;
        position: fixed;
        background-color: #222;
        top:0;
        overflow: hidden;
        transition: all .2s;
    }
    #principal{
        width: 300px;
        left: -300px;
    }
    ul{
        list-style:none;
    }
    ul li a {
        display: block;
        font-size: 18px;
        font-family: 'Arial';
        padding: 10px;
        border-bottom: solid 1px #000;
        color: #ccc;
        text-decoration: none;
        transition: all .2s;
    }
    ul li a:hover{
        background-color: #5b859a;
    }

    ul li span{

    }

    li{
        text-align: justify;
        list-style-type:none;
    }

    .nb-footer{
        background: #272727;
        margin-top: 60px;
        padding-bottom: 30px;
    }
    .nb-footer .footer-single{
        margin-top: 30px;
    }
    .nb-footer .footer-title{
        display: block;
        margin: 10px 0 25px 0;
        border-bottom: 1px dotted #e4e9f0;
    }
    .nb-footer .footer-single a{
        text-decoration: none;
    }

    .nb-footer .footer-single h2{
        color: #eee;
        font-size: 18px;
        font-weight: 200;
        display: inline-block;
        border-bottom: 2px solid #7BF;
        padding-bottom: 5px;
        margin-bottom: -2px;
    }
    .nb-footer .footer-single li{
        border-top: solid 1px #353535;
    }
    .nb-footer .footer-single li:first-child{
        border-top: none;
    }
    .nb-footer .footer-single li a{
        color: #979797;
        font-size: 12px;
        padding: 6px 0px;
        display: block;
        transition:all 0.4s ease-in-out;
    }
    .nb-footer .footer-single li a:hover{
        color: #7BF;
    }
    .nb-footer .footer-single li a:hover i{
        color: #7BF;
    }
    .nb-footer .dummy-logo {
        margin-top: 11px;
        padding-bottom: 9px;
    }
    .nb-footer .dummy-logo .icon {
        margin-right: 10px;
        border-radius: 20px;
        margin-top: 24px;
    }
    .nb-footer .brand {
        background: #7BF;
    }
    .nb-footer .dummy-logo i {
        font-size: 50px;
        color: #fff;
        padding: 5px;
    }
    .nb-footer .dummy-logo p {
        color: #999;
        font-size: 12px;
    }
    .nb-footer .dummy-logo h2 {
        font-size: 24px !important;
        border-bottom: none;
        color: #696969;
        padding: 5px 0;
    }
    .nb-footer .btn-footer{
        border: 1px solid #7BF;
        margin-top: 10px;
        color: #999;
    }
    .nb-footer .btn-footer:hover{
        background: #7BF;
        color: #fff;
        transition:all 0.4s ease-in-out;

    }
    .nb-footer .useful-links li a{
        text-transform: uppercase;
    }
    .nb-footer .footer-project a{
        font-size: 13px;
    }
    .nb-footer .footer-project img{
        margin-bottom: 20px;
        border: 1px solid #666;
        border-radius: 6px;
        padding: 1px;
        opacity: 0.7;
        transition:all 0.4s ease-in-out;
    }
    .nb-footer .footer-project img:hover{
        opacity: 1.0;
        cursor: pointer;
    }
    .nb-footer .footer-project .footer-title{
        margin-top: 0;
    }
    .nb-footer .footer-single p, .footer-single address{
        color: #979797;
        font-size: 14px;
        margin-top: 5px;
        line-height: 22px;
    }
    .nb-copyright{
        background: #171717;
        padding-bottom: 10px;
    }
    .nb-copyright .copyrt{
        margin-top: 22px;
        font-size: 14px;
    }
    .nb-copyright .copyrt a{
        color: #7BF;
    }
    .nb-copyright .footer-social{
        margin-top: 10px;
    }
    .nb-copyright .footer-social i{
        padding: 5px 10px;
        color: #999;
        border: 1px solid #333;
        margin-top: 10px;
        font-size: 20px;
        border-radius: 5px;
        transition:all 0.4s ease-in-out;
    }
    .nb-copyright .footer-social i:hover{
        background: #7BF;
        color: #fff;
    }
    .nb-copyright .footer-social .fa-facebook{
        padding: 5px 14px;
    }

    .modal-body{
        width: 100%;
    }
    .modal-header{
        width: 100%;
    }
    .modal-content{
        width:50%;
    }
    .carregando{
        color:#ff0000;
        display:none;
    }

</style>

<head>
    <script src="jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="jquery.maskedinput.min.js" type="text/javascript"></script>
</head>

<body>

<header>Anúncios</header>

<input type="checkbox" id="chk">
<label for="chk" c class="menu-icon">&#9776&nbsp;&nbsp;&nbsp;Menu</label>

<div class="bg"></div>

<nav class="menu" id="principal">

    <ul>
        <li><a href="">Voltar</a></li>
        <li><a href="index.php">Home</a></li>
        <li><a href="deslogar.php">Deslogar</a></li>
    </ul>

</nav><br><br><br><br><br><br>


<div class="container theme-showcase" role="main">
    <div class="page-header">
        <h1>Cadastrar produtos</h1>
    </div>
    <!-- Inicio Modal -->
    <div class="modal fade" id="myModalcad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Login</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="login.php" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nome:</label>
                            <input name="nome" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="text" class="control-label">Senha:</label>
                            <input name="senha" type="password" class="form-control">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Logar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><br><br>


<form name="signup" enctype="multipart/form-data" method="post" action="salva_cadastroproduto.php">
    <input type="hidden" name="habilitado" value="7">

    <label class="control-label">Nome</label><br><input type="text" name="nome_produto"/><br><br>
    <label class="control-label">Valor</label><br><input type="text" name="valor_produto"/><br><br>
    <label class="control-label">Telefone</label><br><input type="text" id="campotelefone" name="telefone_produto"/><br><br>

    <?php include_once("banco.php");mb_internal_encoding("UTF-8");
    mb_http_output( "iso-8859-1" );
    ob_start("mb_output_handler");
    header('Content-Type: text/html; charset=UTF-8'); ?>

    <label class="control-label">Estado:</label>
    <select name="id_estado" id="id_estado">
        <option value="">Escolha o Estado</option>
        <?php
        $result_cat_post = "SELECT * FROM estado ORDER BY nome";
        $resultado_cat_post = mysqli_query($conn1, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
            echo '<option value="'.$row_cat_post['id'].'">'.utf8_encode($row_cat_post['nome']).'</option>';
        }
        ?>
    </select><br><br>

    <label class="control-label">Cidade:</label>
    <span class="carregando">Aguarde, carregando...</span>
    <select name="id_cidade" id="id_cidade">
        <option value="">Escolha a Cidade</option>
    </select><br><br>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jquery", "1.4.2");
    </script>

    <script type="text/javascript">
        $(function(){
            $('#id_estado').change(function(){
                if( $(this).val() ) {
                    $('#id_cidade').hide();
                    $('.carregando').show();
                    $.getJSON('sub_categorias_post.php?search=',{id_estado: $(this).val(), ajax: 'true'}, function(j){
                        var options = '<option value="">Escolha Cidade</option>';
                        for (var i = 0; i < j.length; i++) {
                            options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                        }
                        $('#id_cidade').html(options).show();
                        $('.carregando').hide();
                    });
                } else {
                    $('#id_cidade').html('<option value="">– Escolha Cidade –</option>');
                }
            });
        });
    </script>

    <script>
        jQuery(function($){
            $("#campotelefone").mask("aaa-9999");
        });
    </script>

    <label class="control-label">Foto 1:</label><br />
    <input type="file" name="foto_produto[]" /><br /><br />

    <label class="control-label">Foto 2:</label><br />
    <input type="file" name="foto_produto[]" /><br /><br />

    <label class="control-label">Foto 3:</label><br />
    <input type="file" name="foto_produto[]" /><br /><br />

    <label class="control-label">Foto 4:</label><br />
    <input type="file" name="foto_produto[]" /><br /><br />

    <input type="submit" value="Cadastrar"><br><br>

</form>


<footer>
    <section class="nb-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-single">
                        <!-- 	<img src="images/logo.png" class="img-responsive" alt="Logo"> -->

                        <!-- This is only for better view of theme if you want your image logo remove div dummy-logo bellow and replace your logo in logo.png and uncomment logo tag above-->
                        <div class="dummy-logo">
                            <div class="icon pull-left brand">
                                <i class="fa fa-copy"></i>
                            </div>
                            <h2>Anúncios</h2>
                            <p>Cadastre seus anúncios</p>
                        </div>

                        <p></p>
                        <a href="" class="btn btn-footer">Leia mais</a>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="footer-single useful-links">
                        <div class="footer-title"><h2>Navegue</h2></div>
                        <ul class="list-unstyled">
                            <li><a href="index.php">Home <i class="fa fa-angle-right pull-right"></i></a></li>
                            <li><a href="about1.html">Sobre <i class="fa fa-angle-right pull-right"></i></a></li>
                            <li><a href="service1.html">Serviços <i class="fa fa-angle-right pull-right"></i></a></li>
                            <li><a href="contact1.html">Contato <i class="fa fa-angle-right pull-right"></i></a></li>
                    </div>
                </div>
                <div class="clearfix visible-sm"></div>

                <div class="col-md-3 col-sm-6">

                    <div class="col-sm-12 left-clear right-clear footer-single footer-project">
                        <div></div>
                        <div class="col-xs-4">

                        </div>
                        <div class="col-xs-4">

                        </div>
                        <div class="col-xs-4">

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-4">

                        </div>
                        <div class="col-xs-4">

                        </div>
                        <div class="col-xs-4">

                        </div>

                    </div>

                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="footer-single">
                        <div class="footer-title"><h2>Endereço de contato</h2></div>
                        <address>
                            323, Bairro, Juiz de Fora<br>
                            MG, Brasil <br>
                            <i class="fa fa-phone"></i>  987 654 3210 <br>
                            <i class="fa fa-fax"></i> 012 123 2345<br>
                            <i class="fa fa-envelope"></i> danielcdequeiroz@gmail.com<br>
                        </address>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="nb-copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 copyrt xs-center">
                    2017 © All Rights Reserved. <a href="">Terms & Conditions</a> | <a href="">Privacy Policy</a>
                </div>
                <div class="col-sm-6 text-right xs-center">
                    <ul class="list-inline footer-social">
                        <li><a href="https://www.facebook.com/groups/anunciojf/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.youtube.com/watch?v=VLylO9noUJs&t=17s"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</footer>
</body>
</html>