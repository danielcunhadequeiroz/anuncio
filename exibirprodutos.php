<?php
include_once('banco.php');

$id = $_GET['id'];
$query= "Select * from produto where id = {$id}";
$result = mysqli_query($conn1,$query);
$exibe = mysqli_fetch_assoc($result);

if(isset($_GET['update']))
{
    echo
    "<script>   
alert('Cadastro alterado com sucesso!');
    </script>";
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='icon' type='image/jpg' href="anuncio.jpg">
    <title>Exibir Produtos</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="base.css" media="screen">
    <link rel="stylesheet" href="https://code.google.com/p/css3-mediaqueries-js">
    <script src="js/jquery-1.7.2.min.js"></script>

    <link href="css/lightbox.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo"></script>

</head>

<style>

        body{
            font-size: 20px;
            background-image: url(background.jpg);
            background-repeat: no-repeat;
            background-position: absolute;
            background-size: cover;
            height: auto;
        }

        img{
            margin-left: 0%;
            margin-top: 0%;
            margin-right: 0%;
            margin-bottom: 0%;

        }

        .modal.and.carousel {
            position: fixed; // Needed because the carousel overrides the position property
        }

        table tr td{
            height: 40px;
            width: 65px;
        }

        header {
            min-height: 60px;
            position: fixed;
            top: 0;
            right: 0;
            left: 20%;
            text-align: center;
            z-index: 2;
        }
        .container{
            width:100%;
            top: 0;
        }

        .control-label{
            color:white;
        }
        h1{
            text-align: center;
            color:white;
        }
        p{
            color:white;
        }
        .control-label{
            color:white;
        }
*{
    margin: 0;
    padding: 0;
}
    .lbox{
        visibility: hidden;
        opacity:0;
    }

    ul{
    width: 880px;
    list-style: none;
    display: flex;
    margin: 100px auto;
    }

    min{
    width: 200px;
    padding: 10px;
    }

    .lbox:target{
        opacity: 1;
        visibility: visible;
        width: 100%;
        height: 100%;
        top:0;
        left:0;
        background: rgba(10,10,10,.7);
        position:fixed;
    }

    .box_img{
        width: 1000px;
        margin: 150px auto;
    }
    .btn{
        color:black;
        text-decoration: none;
        position: absolute;
        font-size:20px;
        text-align:center;
    }
    #prev{
        left:5%;
        top:45%;
    }
        #next{
            right:5%;
            top:45%;
        }
</style>

<body>

<h1 style="text-align:center;">Exibir Produto</h1>

<br>


<div class="container">

    <div id="meuSlider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li href="#" data-target="#meuSlider" data-slide-to="0" class="active"></li>
            <li data-target="#meuSlider" data-slide-to="1"></li>
            <li data-target="#meuSlider" data-slide-to="2"></li>
            <li data-target="#meuSlider" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div href="#lightbox" data-toggle="modal" class="item active"><img src="<?php echo $exibe['foto_produto1']; ?>"  alt="Slider 1" style="margin:0 auto;"/></div>
            <div href="#lightbox" data-toggle="modal" class="item"><img src="<?php echo $exibe['foto_produto2']; ?>" alt="Slide 2" style="margin:0 auto;" /></div>
            <div href="#lightbox" data-toggle="modal" class="item"><img src="<?php echo $exibe['foto_produto3']; ?>" alt="Slide 3" style="margin:0 auto;"/></div>
            <div href="#lightbox" data-toggle="modal" class="item"><img src="<?php echo $exibe['foto_produto4']; ?>" alt="Slide 4" style="margin:0 auto;" /></div>
        </div>
        <a class="left carousel-control" href="#meuSlider" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#meuSlider" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    <br><br>


    <div class="modal fade and carousel slide" id="lightbox">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <ol class="carousel-indicators">
                        <li data-target="#lightbox" data-slide-to="0" class="active"></li>
                        <li data-target="#lightbox" data-slide-to="1"></li>
                        <li data-target="#lightbox" data-slide-to="2"></li>
                        <li data-target="#lightbox" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<?php echo $exibe['foto_produto1']; ?>" alt="First slide" style="margin:0 auto;"/></>
                        </div>
                        <div class="item">
                            <img src="<?php echo $exibe['foto_produto2']; ?>" alt="Second slide" style="margin:0 auto;"/></>
                        </div>
                        <div class="item">
                            <img src="<?php echo $exibe['foto_produto3']; ?>" alt="Third slide" style="margin:0 auto;"/></>
                        </div>
                        <div class="item">
                            <img src="<?php echo $exibe['foto_produto4']; ?>" alt="Four slide" style="margin:0 auto;"/></>
                            <div class="carousel-caption"><p>Notebook</p></div>
                        </div>
                    </div><!-- /.carousel-inner -->
                    <a class="left carousel-control" href="#lightbox" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#lightbox" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div><!-- /.container -->

<div>
    <form name="signup" enctype="multipart/form-data" method="post" action="">
        <label class="control-label"><?= $exibe['nome_produto']; ?></label><br><br>
        <label class="control-label"><?= $exibe['valor_produto']; ?><br><br>
            <label class="control-label"><?= $exibe['telefone_produto']; ?></label><br><br>

        <input  type="hidden" name="id" value="<?= $exibe['id']; ?>"  />
    </form>
</div>


<form action="produto.php">
    <button class="btn btn-primary">Voltar</button>
</form><br><br>

</body>
</html>





